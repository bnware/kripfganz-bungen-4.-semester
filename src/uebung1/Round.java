package uebung1;

import java.io.BufferedReader;

import util.Console;
import util.Launcher;

/**
 * @author Nicky Gruner
 */
public class Round implements Console {

    private static final String SHELL = "Round";

    @Override
    public void process (String shell, String[] params, BufferedReader in) {
        if (!SHELL.equals(shell) && params.length == 0) {
            Launcher.ownShell(SHELL, in, this);
            return;
        }
        if (params.length != 2) {
            if (SHELL.equals(shell)) {
                throw new IllegalArgumentException("usage: <x> <t>");
            }
            throw new IllegalArgumentException("usage: Round <x> <t>");
        }
        System.out.printf("%f%n", round(Double.parseDouble(params[0]), Integer.parseInt(params[1])));
    }

    public static double round (double x, int t) {
        if (x == 0) {
            return x;
        }
        int e = (int) Math.floor(Math.log10(Math.abs(x))) + 1;
        x *= Math.pow(10, t-e);
        x = Math.round(x);
        x *= Math.pow(10, e-t);
        return x;
    }
    
}
