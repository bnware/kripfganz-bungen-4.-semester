package uebung1;

import java.io.BufferedReader;

import util.Console;
import util.Launcher;


/**
 * @author Benjamin Jesuiter
 */
public class Integral_Aufg3 implements Console {

    private static final String SHELL = "Integral_Aufg3";

    @Override
    public void process(String shell, String[] params, BufferedReader in) {
        if (!SHELL.equals(shell) && params.length == 0) {
            Launcher.ownShell(SHELL, in, this);
            return;
        }
        if (params.length != 2) {
            if (SHELL.equals(shell)) {
                throw new IllegalArgumentException("usage: <maxRecursion> <mantissenlaenge>");
            }
            throw new IllegalArgumentException("usage: Integral_Aufg3 <maxRecursion> <mantissenlaenge>");
        }
        calcIntegral(Integer.parseInt(params[0]), Integer.parseInt(params[1]));
    }

    /**
     * @param maxRecursion    default = 20
     * @param mantissenlength Lünge der Mantisse mit der gerechnet werden muss
     * @return double Ergebnis
     */
    private double calcIntegral(int maxRecursion, int mantissenlength) {

        double i = Math.log(6.0 / 5.0);

        for (int n = 1; n <= maxRecursion; n++) {
            i = -5.0 * i + 1.0 / n;
            i = Round.round(i, mantissenlength);
            System.out.println(i);
        }

        return i;
    }
}
