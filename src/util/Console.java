package util;

import java.io.BufferedReader;

/**
 * @author Nicky Gruner
 */
public interface Console {

    void process (String shell, String[] params, BufferedReader in);
    
}
