package util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nicky Gruner
 */
public class Launcher {

    private static final String SHELL = "uebung";
    private static String pack = "";

    public static void main (String[] args) {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print(SHELL);
            System.out.print('>');
            String[] params;
            try {
                String line = in.readLine();
                if (line == null) {
                    break;
                }
                if (line.isEmpty()) {
                    continue;
                }
                params = parseArguments(line);
                if ("quit".equals(params[0])) {
                    break;
                } else if ("package".equals(params[0])) {
                    switch (params.length) {
                        case 1:
                            System.out.println(pack);
                            break;
                        case 2:
                            pack = params[1];
                            break;
                        default:
                            throw new IllegalArgumentException("usage: package <qualified_package_name>");
                    }
                } else {
                    Console console = getConsole(params[0]);
                    console.process(SHELL, Arrays.copyOfRange(params, 1, params.length), in);
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {}
            }
        }
    }

    public static void ownShell (String name, BufferedReader in, Console console) {
        while (true) {
            System.out.print(name);
            System.out.print('>');
            try {
                String line = in.readLine();
                if (line == null) {
                    return;
                }
                if (line.isEmpty()) {
                    continue;
                }
                String[] params = parseArguments(line);
                if ("quit".equals(params[0])) {
                    return;
                } else {
                    console.process(name, params, in);
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {}
            }
        }
    }

    private static String[] parseArguments (String line) {
        List<String> args = new LinkedList<>();
        StringBuilder buffer = new StringBuilder();
        boolean escape = false;
        boolean string = false;
        loop:
        for (char c : line.toCharArray()) {
            switch (c) {
                case '\\':
                    if (!escape) {
                        escape = true;
                        continue loop;
                    }
                case '"':
                    if (!escape) {
                        string = !string;
                        continue loop;
                    }
                    break;
                case ' ':
                    if (!string) {
                        args.add(buffer.toString());
                        buffer = new StringBuilder();
                        continue loop;
                    }
                    break;

            }
            buffer.append(c);
            escape = false;
        }
        if (buffer.length() > 0) {
            args.add(buffer.toString());
        }
        return args.toArray(new String[args.size()]);
    }

    private static Console getConsole (String name) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException,
                                                           InstantiationException
    {
        if (!pack.isEmpty()) {
            name = pack + "." + name;
        }
        return Class.forName(name).asSubclass(Console.class).getConstructor().newInstance();
    }
    
}
