package uebung2;

import java.io.BufferedReader;

import util.Console;
import util.Launcher;

/**
 * @author Nicky Gruner
 */
public class Aufg2 implements Console {

    private static final String SHELL = "Aufg2";

    @Override
    public void process(String shell, String[] params, BufferedReader in) {
        if (!SHELL.equals(shell) && params.length == 0) {
            Launcher.ownShell(SHELL, in, this);
            return;
        }
        if (params.length != 3) {
            if (SHELL.equals(shell)) {
                throw new IllegalArgumentException("usage: <matrix in json> <mantisse> <bool spaltenpivot>");
            }
            throw new IllegalArgumentException("usage: Aufg2 <matrix in json> <mantisse> <bool spaltenpivot>");
        }
    }

//    List<Double> calcMatrix ( ) {
//
//    }


}
