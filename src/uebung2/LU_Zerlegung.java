package uebung2;

import java.io.BufferedReader;

import uebung1.Round;
import util.Console;
import util.Launcher;

/**
 * @author Benjamin Jesuiter
 */
public class LU_Zerlegung implements Console {

    private static final String SHELL = "LU_Zerlegung";

    @Override
    public void process (String shell, String[] params, BufferedReader in) {
        if (!SHELL.equals(shell) && params.length == 0) {
            Launcher.ownShell(SHELL, in, this);
            return;
        }
        String usage = String.format("Usage: %s <matrix a> <mantisse> [<pivot>]", SHELL.equals(shell) ? "" : SHELL);
        boolean pivot = true;
        int mantisse;
        double[][] matrix;
        switch (params.length) {
            case 3:
                pivot = Boolean.parseBoolean(params[2]);
            case 2:
                mantisse = Integer.parseInt(params[1]);
                matrix = LU.parseMatrix(params[0]);
                break;
            default:
                throw new IllegalArgumentException(usage);
        }
        LU lu = decomposeLU(matrix, mantisse, pivot);
        System.out.println(lu.toString(mantisse));
    }

    public static LU decomposeLU (double[][] matrix, int mantisse, boolean pivot) {
        int n = matrix.length;
        double[][] a = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = Round.round(matrix[i][j], mantisse);
            }
        }
        LU lu = new LU(n);
        for (int k = 0; k < n; k++) {
            if (pivot) {
                int m = selectPivot(a, k);
                if (m != k) {
                    permuteRows(a, lu, m, k);
                }
            } else if (a[k][k] == 0) {
                for (int i = k + 1; i < n; i++) {
                    if (a[i][k] != 0) {
                        permuteRows(a, lu, i, k);
                        break;
                    }
                }
            }
            for (int i = k; i < n; i++) {
                lu.l[i][k] = Round.round(a[i][k] / a[k][k], mantisse);
                if (i == k) {
                    continue;
                }
                for (int j = k; j < n; j++) {
                    a[i][j] = Round.round(a[i][j] - Round.round(lu.l[i][k] * a[k][j], mantisse), mantisse);
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                lu.u[i][j] = Round.round(a[i][j], mantisse);
            }
        }
        return lu;
    }

    private static int selectPivot (double[][] a, int k) {
        int n = a.length;
        int m = k;
        double max = Math.abs(a[k][k]);
        for (int i = k + 1; i < n; i++) {
            double akk = Math.abs(a[i][k]);
            if (akk > max) {
                max = akk;
                m = i;
            }
        }
        return m;
    }

    private static void permuteRows (double[][] a, LU lu, int r1, int r2) {
        permuteRows(a, r1, r2);
        permuteRows(lu.l, r1, r2);
        permuteRows(lu.u, r1, r2);
        permuteRows(lu.rows, r1, r2);
    }

    private static <T> void permuteRows (T[] a, int r1, int r2) {
        T temp = a[r1];
        a[r1] = a[r2];
        a[r2] = temp;
    }

}
