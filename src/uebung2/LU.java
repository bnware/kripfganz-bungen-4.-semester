package uebung2;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Benjamin Jesuiter
 */
public class LU {

    double[][] l;
    double[][] u;
    Integer[] rows;

    public LU (int dimension) {
        l = new double[dimension][dimension];
        u = new double[dimension][dimension];
        rows = new Integer[dimension];
        for (int i = 0; i < dimension; i++) {
            rows[i] = i;
        }
    }

    public double[][] getL () {
        return l;
    }

    public double[][] getU () {
        return u;
    }

    public Integer[] getRows () {
        return rows;
    }

    @Override
    public String toString () {
        return toString(10);
    }

    public String toString (int mantisse) {
        StringBuilder builder = new StringBuilder();
        builder.append("l = [");
        printMatrix(builder, l, mantisse);
        builder.append("]\n\n");
        builder.append("u = [");
        printMatrix(builder, u, mantisse);
        builder.append("]\n\n");
        builder.append("rows = ");
        builder.append(Arrays.deepToString(rows));
        return builder.toString();
    }

    public static void printMatrix (StringBuilder builder, double[][] matrix, int mantisse) {
        for (int i = 0; i < matrix.length; i++) {
            if (i != 0) {
                builder.append("\n     ");
            }
            for (int j = 0; j < matrix.length; j++) {
                if (j != 0) {
                    builder.append(" ");
                }
                builder.append(String.format(String.format("%%%df", mantisse), matrix[i][j]));
            }
        }
    }

    public static double[][] parseMatrix (String s) {
        char[] chars = s.toCharArray();
        List<List<Double>> buffer = new LinkedList<>();
        StringBuilder builder = new StringBuilder();
        List<Double> line = null;
        int cols = 0;
        boolean row = false;
        for (char c : chars) {
            switch (c) {
                case '[':
                    if (row) {
                        throw new IllegalArgumentException("invalid matrix");
                    }
                    line = new LinkedList<>();
                    builder.setLength(0);
                    row = true;
                    break;
                case ',':
                    if (!row) {
                        break;
                    }
                    line.add(new Double(builder.toString().trim()));
                    builder.setLength(0);
                    break;
                case ']':
                    if (!row) {
                        throw new IllegalArgumentException("invalid matrix");
                    }
                    line.add(new Double(builder.toString().trim()));
                    cols = Math.max(cols, line.size());
                    buffer.add(line);
                    row = false;
                    break;
                default:
                    builder.append(c);
            }
        }
        int rows = buffer.size();
        double[][] matrix = new double[rows][cols];
        for (int i = 0; i < rows; i++) {
            line = buffer.get(i);
            for (int j = 0; j < line.size(); j++) {
                matrix[i][j] = line.get(j);
            }
        }
        return matrix;
    }
}
